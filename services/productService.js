myApp.service('productService', function() {
    
    this.getCategory = function(category) {
        console.info('Entring getCategory.');
        
        console.debug('Loading products for category: ' + category);
        
        if (this.categories[category] !== undefined) {
            console.debug('Category title: ' + this.categories[category].categoryTitle);
            console.debug('Produts in this category: ' + this.categories[category].products.length);
            
            return this.categories[category];
        } else {
            console.debug('There is no such category');
            return undefined;
        }
    }
    
    this.categories = new Array();
    
    this.categories['men-dress-shoes'] = {};
    this.categories['men-dress-shoes'].categoryTitle = 'Men Dress Shoes';
    this.categories['men-dress-shoes'].products = [
        {
            name: 'Ben Black PU',
            imageUrl: 'assets/men-dress-shoes/ben-black-pu.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'David Black PU',
            imageUrl: 'assets/men-dress-shoes/david-black-pu.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Grant Black PU',
            imageUrl: 'assets/men-dress-shoes/grant-black-pu.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Mark Boy Black PU',
            imageUrl: 'assets/men-dress-shoes/mark-boy-black-pu.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Ragen Black PU Men & Boy',
            imageUrl: 'assets/men-dress-shoes/ragen-black-pu-men-&-boy.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Wallace Black PU Men & Boy',
            imageUrl: 'assets/men-dress-shoes/wallace-black-pu-men-and-boy.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        }    
    ];
    
    this.categories['ball-band-women-loafers'] = {};
    this.categories['ball-band-women-loafers'].categoryTitle = 'Ball-Band Women Loafers';
    this.categories['ball-band-women-loafers'].products = [
        {
            name: 'Dazzle 20 Beige',
            imageUrl: 'assets/ball-band-women-loafers/dazzle-20-beige.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Dazzle 20 Black',
            imageUrl: 'assets/ball-band-women-loafers/dazzle-20-black.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Dazzle 20 Brown',
            imageUrl: 'assets/ball-band-women-loafers/dazzle-20-brown.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Dazzle 20 Mint',
            imageUrl: 'assets/ball-band-women-loafers/dazzle-20-mint.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Dazzle 20 Red',
            imageUrl: 'assets/ball-band-women-loafers/dazzle-20-red.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Feliz 12 Beige PU',
            imageUrl: 'assets/ball-band-women-loafers/feliz-12-beige-pu.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Feliz 12 Black PU',
            imageUrl: 'assets/ball-band-women-loafers/feliz-12-black-pu.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Feliz 12 Brown PU',
            imageUrl: 'assets/ball-band-women-loafers/feliz-12-brown-pu.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Feliz 12 Mint PU',
            imageUrl: 'assets/ball-band-women-loafers/feliz-12-mint-pu.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Feliz 12 Red PU',
            imageUrl: 'assets/ball-band-women-loafers/feliz-12-red-pu.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Fine 17 Beige',
            imageUrl: 'assets/ball-band-women-loafers/fine-17-beige.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Fine 17 Black PU',
            imageUrl: 'assets/ball-band-women-loafers/fine-17-black-pu.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Fine 17 Brown',
            imageUrl: 'assets/ball-band-women-loafers/fine-17-brown.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Glee 18 Black PU',
            imageUrl: 'assets/ball-band-women-loafers/glee-18-black-pu.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Glee 18',
            imageUrl: 'assets/ball-band-women-loafers/glee-18.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Glee 18',
            imageUrl: 'assets/ball-band-women-loafers/glee-18.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Life 13 Beige PU',
            imageUrl: 'assets/ball-band-women-loafers/life-13-beige-pu.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Life 13 Black PU',
            imageUrl: 'assets/ball-band-women-loafers/life-13-black-pu.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Life 13 Brown PU',
            imageUrl: 'assets/ball-band-women-loafers/life-13-brown-pu.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Light 26 Black PU',
            imageUrl: 'assets/ball-band-women-loafers/light-26-black-pu.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Shine 19 Beige',
            imageUrl: 'assets/ball-band-women-loafers/shine-19-beige.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Shine 19 Black',
            imageUrl: 'assets/ball-band-women-loafers/shine-19-black.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Shine 19 Brown',
            imageUrl: 'assets/ball-band-women-loafers/shine-19-brown.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Shine 19 Mint',
            imageUrl: 'assets/ball-band-women-loafers/shine-19-mint.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Shine 19 Red',
            imageUrl: 'assets/ball-band-women-loafers/shine-19-red.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Spark 15 Beige PU',
            imageUrl: 'assets/ball-band-women-loafers/spark-15-beige-pu.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Spark 15 Black PU',
            imageUrl: 'assets/ball-band-women-loafers/spark-15-black-pu.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        {
            name: 'Spark 15 Brown PU',
            imageUrl: 'assets/ball-band-women-loafers/spark-15-brown-pu.jpg',
            labels: [],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        }     
    ];
    
    
    this.categories['men-ball-band'] = {};
    this.categories['men-ball-band'].categoryTitle = 'Men\'s Ball-Band';
    this.categories['men-ball-band'].products = [
        {
            name: 'product-name',
            imageUrl: 'images/test.jpg',
            labels: ['hot'],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        
        {
            name: 'Ben Black PU',
            imageUrl: 'images/test2.jpg',
            labels: [],
            description: 'Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.'
            ]
        }
    ];
    
    this.categories['ball-band-women-comforts'] = {};
    this.categories['ball-band-women-comforts'].categoryTitle = 'Ball-Band Women Comforts';
    this.categories['ball-band-women-comforts'].products = [
        {
            name: 'Grace 07 Black PU',
            imageUrl: 'assets/ball-band-women-comforts/grace-07-black-pu.jpg',
            labels: ['hot'],
            description: 'Pellentesque luctus pulvinar mi, vel sodales sem finibus in. Donec at nibh sed ligula faucibus suscipit vel nec mi. Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.',
                'Donec vel dui eget nisl feugiat lacinia.',
                'Duis ut nulla at nibh auctor accumsan at in lorem.'
            ]
        },
        
        {
            name: 'Inspire 02 Black PU',
            imageUrl: 'assets/ball-band-women-comforts/inspire-02-black-pu.jpg',
            labels: [],
            description: 'Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.'
            ]
        },
        
        {
            name: 'Passion 09 Black PU',
            imageUrl: 'assets/ball-band-women-comforts/passion-09-black-pu.jpg',
            labels: [],
            description: 'Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.'
            ]
        },
        
        {
            name: 'Peaceful 08 Beige PU',
            imageUrl: 'assets/ball-band-women-comforts/peaceful-08-beige-pu.jpg',
            labels: [],
            description: 'Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.'
            ]
        },
        
        {
            name: 'Peaceful 08 Black',
            imageUrl: 'assets/ball-band-women-comforts/peaceful-08-black.jpg',
            labels: [],
            description: 'Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.'
            ]
        },
        
        {
            name: 'Peaceful 08 Brown PU',
            imageUrl: 'assets/ball-band-women-comforts/peaceful-08-brown-pu.jpg',
            labels: [],
            description: 'Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.'
            ]
        },
        
        {
            name: 'Peaceful 08 White PU',
            imageUrl: 'assets/ball-band-women-comforts/peaceful-08-white-pu.jpg',
            labels: [],
            description: 'Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.'
            ]
        },
        
        {
            name: 'Relax 05 Black PU',
            imageUrl: 'assets/ball-band-women-comforts/relax-05-black-pu.jpg',
            labels: [],
            description: 'Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.'
            ]
        },
        
        {
            name: 'Viva 06 Black PU',
            imageUrl: 'assets/ball-band-women-comforts/viva-06-black-pu.jpg',
            labels: [],
            description: 'Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.'
            ]
        },
        
        {
            name: 'W020 Beige PU',
            imageUrl: 'assets/ball-band-women-comforts/w020-beige-pu.jpg',
            labels: [],
            description: 'Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.'
            ]
        },
        
        {
            name: 'W020 Black PU',
            imageUrl: 'assets/ball-band-women-comforts/w020-black-pu.jpg',
            labels: [],
            description: 'Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.'
            ]
        },
        
        {
            name: 'W020 Brown PU',
            imageUrl: 'assets/ball-band-women-comforts/w020-brown-pu.jpg',
            labels: [],
            description: 'Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.'
            ]
        },
        
        {
            name: 'W020 White PU',
            imageUrl: 'assets/ball-band-women-comforts/w020-white-pu.jpg',
            labels: [],
            description: 'Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.'
            ]
        },
        
        {
            name: 'W023 Beige PU',
            imageUrl: 'assets/ball-band-women-comforts/w020-beige-pu.jpg',
            labels: [],
            description: 'Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.'
            ]
        },
        
        {
            name: 'W023 Black PU',
            imageUrl: 'assets/ball-band-women-comforts/w020-black-pu.jpg',
            labels: [],
            description: 'Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.'
            ]
        },
        
        {
            name: 'W023 Brown PU',
            imageUrl: 'assets/ball-band-women-comforts/w020-brown-pu.jpg',
            labels: [],
            description: 'Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.'
            ]
        },
        
        {
            name: 'W023 White PU',
            imageUrl: 'assets/ball-band-women-comforts/w020-white-pu.jpg',
            labels: [],
            description: 'Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.'
            ]
        },
        
        {
            name: 'W025 Beige PU',
            imageUrl: 'assets/ball-band-women-comforts/w025-beige-pu.jpg',
            labels: [],
            description: 'Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.'
            ]
        },
        
        {
            name: 'W025 Black PU',
            imageUrl: 'assets/ball-band-women-comforts/w025-black-pu.jpg',
            labels: [],
            description: 'Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.'
            ]
        },
        
        {
            name: 'W025 Brown PU',
            imageUrl: 'assets/ball-band-women-comforts/w025-brown-pu.jpg',
            labels: [],
            description: 'Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.'
            ]
        },
        
        {
            name: 'W025 White PU',
            imageUrl: 'assets/ball-band-women-comforts/w025-white-pu.jpg',
            labels: [],
            description: 'Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.'
            ]
        },
        
        {
            name: 'W028 Black PU',
            imageUrl: 'assets/ball-band-women-comforts/w028-black-pu.jpg',
            labels: [],
            description: 'Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.'
            ]
        },
        
        {
            name: 'W052 Beige PU',
            imageUrl: 'assets/ball-band-women-comforts/w052-beige-pu.jpg',
            labels: [],
            description: 'Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.'
            ]
        },
        
        {
            name: 'W052 Black PU',
            imageUrl: 'assets/ball-band-women-comforts/w052-black-pu.jpg',
            labels: [],
            description: 'Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.'
            ]
        },
        
        {
            name: 'W052 Brown PU',
            imageUrl: 'assets/ball-band-women-comforts/w052-brown-pu.jpg',
            labels: [],
            description: 'Praesent consequat quis erat non sodales.',
            bulletPoints: [
                'Proin auctor lorem nec nisl lobortis, vitae auctor nibh luctus.',
                'Curabitur ac tortor nec dui sollicitudin facilisis et id justo.',
                'Nunc viverra ligula consectetur sem euismod tempor.',
                'Pellentesque maximus neque ac efficitur suscipit.'
            ]
        }
    ]
});