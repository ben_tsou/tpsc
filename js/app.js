var myApp = angular.module('MyApp', ['ngRoute']);

myApp.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'main.html',
            controller: 'mainController'
        })
        .when('/about', {
            templateUrl: 'about.html',
            controller: 'aboutController'
        })
        .when('/contact', {
            templateUrl: 'contact.html',
            controller: 'contactController'
        })
        .when('/catalog', {
            templateUrl: 'catalog.html',
            controller: 'catalogController'
        });
    
});

myApp.directive('productSection', function () {
    return {
        templateUrl: '../directives/product-section.html',
        replace: true,
        scope: {
            productObject: "="
        }
    };
});

myApp.controller('navController', ['$scope', '$log', 'productService', function ($scope, $log, productService) {
    
    $scope.productCatalog = [
        {desc: "Ball-Band Women Comforts", category: "ball-band-women-comforts"},
        {desc: "Ball-Band Women Loafers", category: "ball-band-women-loafers"},
        {desc: "Men Dress Shoes", category: "men-dress-shoes"}
        
//        {desc: "Men's Ball-Band", category: "men-ball-band"},
//        {desc: "Men's Ball-Band Sandals", category: "men-ball-band-sandals"},
//        {desc: "Women's Ball-Band", category: "women-ball-band"},
//        {desc: "Women's Ball-Band Sandals", category: "women-ball-band-sandals"},
//        {desc: "Women's Sandals", category: "women-sandals"},
//        {desc: "Infant & Kids Flats", category: "infant-kids-flats"},
//        {desc: "DEK", category: "dek"},
//        {desc: "Plain Pumps", category: "plain-pumps"},
//        {desc: "N-Style Flats", category: "n-style-flats"},
//        {desc: "R-Style Flats", category: "r-style-flats"},
//        {desc: "JB Signature Flats", category: "jb-signature-flats"},
//        {desc: "Back to School", category: "back-tos-school"}
    ];

}]);

myApp.controller('mainController', ['$scope', '$log', function ($scope, $log) {

}]);

myApp.controller('aboutController', ['$scope', '$log', function ($scope, $log) {

}]);

myApp.controller('contactController', ['$scope', '$log', function ($scope, $log) {

}]);

myApp.controller('catalogController',
                 ['$scope', '$log', '$routeParams', 'productService',
                  function ($scope, $log, $routeParams, productService) {
    
    $log.debug($routeParams);
                      
    var category = $routeParams.category;
    $log.debug('Product category: ' + category);

    $scope.currentCategory = productService.getCategory(category);
}]);
