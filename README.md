# The People's Shoe Company Webapp #

This is the webapp for The People's Shoe Company

## Deployment

This webapp is deployed on S3 and distributed via CloudFront.

Assume role to TPSC:
```aws --profile tpsc sts assume-role --role-arn arn:aws:iam::822069430632:role/Admin --role-session-name tpsc-temp --duration-seconds 3600```

To upload to S3, use the aws cli command:
```aws --profile tpsc-temp s3 sync . s3://tpsc-webapp --delete --exclude ".git/*"```

